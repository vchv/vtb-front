(function() {
  'use strict';

  Polymer({
    is: 'cells-modal-alert',

    behaviors: [
      CellsBehaviors.i18nBehavior
    ],

    properties: {
      /**
       * Message type.<br>
       * Used for styling purposes and to set the icon `aria-label`.<br>
       * Can be one of these: `alert`, `warning`, `info`, `success`.<br>
       * In `toast` mode it sets the background color.
       */
      type: {
        type: String,
        reflectToAttribute: true
      },

      /**
       * ID of the warning or alert icon.
       */
      icon: {
        type: String,
        value: null
      },

      /**
       * Size of the icon.
       */
      iconSize: {
        type: Number,
        value: 26
      },

      /**
       * Alternative text for the icon (`aria-label`).<br>
       * The alternative text is automatically set depending on the `type` attribute if your are
       * using on of these types: `alert`, `warning`, `info`.<br>
       * `success` type does not have a corresponding `aria-label`.
       */
      iconLabel: {
        type: String,
        value: null
      },

      /**
       * Text content. HTML tags are allowed.
       */
      content: {
        type: String
      },

      /**
       * Text for the first button.
       */
      firstButtonLabel: {
        type: String,
        value: null
      },

      /**
       * Text for the second button.
       */
      secondButtonLabel: {
        type: String,
        value: null
      },

      /**
       * Classes for the first button.
       */
      firstButtonClasses: {
        type: String,
        value: null
      },

      /**
       * Classes for the second button.
       */
      secondButtonClasses: {
        type: String,
        value: null
      },

      /**
       * Disables the first button.
       */
      firstButtonDisabled: {
        type: Boolean,
        value: false
      },

      /**
       * Disables the second button.
       */
      secondButtonDisabled: {
        type: Boolean,
        value: false
      },

      /**
       * Set to true to close the modal after clicking a button.
       */
      closeOnButtonClick: {
        type: Boolean,
        value: false
      },

      /**
       * Set to true to open the modal.
       */
      opened: {
        type: Boolean,
        value: false,
        notify: true,
        observer: '_toggleOpen',
        reflectToAttribute: true
      },

      /**
       * Set to true to disable closing the modal by clicking on the overlay (backdrop).
       */
      noCancelOnOutsideClick: {
        type: Boolean,
        value: false
      },

      /**
       * Set to true to not display the overlay (backdrop).
       */
      noBackdrop: {
        type: Boolean,
        value: false
      },

      /**
       * Set to true to restore the focus to the element that opened the modal (`<button>`, `<a>`, etc.)
       */
      restoreFocusOnClose: {
        type: Boolean,
        value: false
      },

      _hasActions: {
        type: Boolean,
        computed: '_computeHasActions(firstButtonLabel, secondButtonLabel)'
      },

      /**
       * Set to true to close the modal after clicking the content.
       */
      closeOnContentClick: {
        type: Boolean,
        value: false
      },

      /**
       * Set to true to make the modal behave like a toast.
       */
      toast: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        observer: '_toastChanged'
      },

      /**
       * Duration in milliseconds to display the toast.
       * Set to zero to display a permanent toast (won't close).
       * Only has effect if the toast property is set to true.
       */
      duration: {
        type: Number,
        value: 6000
      }
    },

    observers: [
      '_closeToast(opened, toast, duration)'
    ],

    /**
     * Opens the modal.
     */
    open: function() {
      this.opened = true;
    },

    /**
     * Closes the modal.
     */
    close: function() {
      this.opened = false;
    },

    _toggleOpen: function(opened, previousState) {
      if (opened) {
        this.$.modal.open();
      } else if (previousState) {
        this.$.modal.close();
      }
    },

    /**
     * Used to sync opened property with cells-bottom-modal opened property
     * without using two way data binding (<cells-bottom-modal opened="{{opened}}">)
     * because iron-overlay-behavior closes the modal abruptly by setting an inline "display: none".
     */
    _setOpened: function(e) {
      this.opened = e.detail.value;
    },

    _onButtonClick: function(e) {
      this.dispatchEvent(new CustomEvent(e.currentTarget.dataset.button + '-button-click', { bubbles: true, composed: true }));

      if (this.closeOnButtonClick) {
        this.opened = false;
      }
    },

    _computeHasActions: function(firstButtonLabel, secondButtonLabel) {
      return Boolean(firstButtonLabel || secondButtonLabel);
    },

    _onContentClick: function(e) {
      if (!this.closeOnContentClick) {
        return;
      }

      var target = Polymer.dom(e).rootTarget;
      var isClickable = ['BUTTON', 'A', 'INPUT'].find(function(tag) {
        return target.nodeName === tag;
      });

      if (!isClickable) {
        this.opened = false;
      }
    },

    /**
     * Automatically set some properties for the toast mode (dangerous).
     */
    _toastChanged: function(toast) {
      this.noCancelOnOutsideClick = toast ? toast : this.noCancelOnOutsideClick;
      this.noBackdrop = toast ? toast : this.noBackdrop;
      this.closeOnContentClick = toast ? !toast : this.closeOnContentClick;
      this.firstButtonClasses = toast ? 'link' : this.firstButtonClasses;
      this.secondButtonClasses = toast ? 'link' : this.secondButtonClasses;
    },

    _closeToast: function(opened, toast, duration) {
      if (opened && toast && duration) {
        this.debounce('close-toast', function() {
          this.opened = false;
        }, duration);
      }
    }

    /**
     * Fired after clicking the first button.
     * @event first-button-click
     */

    /**
     * Fired after clicking the second button.
     * @event second-button-click
     */

  });
}());
